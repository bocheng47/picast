<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-Picast">About The Picast</a>
      <ul>
        <li><a href="#flowchart">FlowChart</a></li>
        <li><a href="#build-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <b>Hardware</b>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#circuit-design">Circuit Design</a></li>
        <b>Software</b>
        <li><a href="#install-package">Install Package</a></li>
        <li><a href="#sequence-diagram">Sequence Diagram</a></li>
        <li><a href="#dht22-module">DHT22 Module</a></li>
        <li><a href="#emotion-detection-module">Emotion Detection Module</a></li>
        <li><a href="#music-recommendation-module">Music Recommendation Module</a></li>
        <li><a href="#combine-all-module">Combine All Module</a></li>
      </ul>
    </li>
    <li>
      <a href="#usage">Usage</a>
      <ul>
        <li><a href="#demo-video">Demo Video</a></li>
      </ul>
    </li>
    <li>
      <a href="#reference">Reference</a>
    </li>
  </ol>
</details>



<!-- ABOUT THE PICAST -->
## About The Picast

<img src="img/logo.PNG">

<b>Picast療癒神器！ 完善回家放鬆的流程，回家也能有賓至如歸的體驗！</b>

到家後最好放鬆的方式，除了是找家裡的毛小孩玩耍，聽音樂也是一個很好的選擇，如果我們想單純地轉換心情，在家就扮演不同的角色，偶爾也會希望打開家門的一瞬間，能有伴奏跟特殊的配樂能出現。

透過Picast偵測環境因素（濕度、溫度）以及情感偵測，做個人化的音樂推薦，Picast的理想是如果您處於刺激的環境中並想放慢速度，我們將選擇安靜的音樂或舒緩的聲音，例如海浪聲和輕快的音樂，這有助於減輕焦慮。如果您希望因為沒有活力而需要刺激時，我們可以提供動感的音樂。

Picast healing artifact! Perfect the experience of going home and relaxing! To relax after arriving home, listening to music is also a good choice or find pets at home to play. Everyone usually wants to simply change our mood and be chill when they are arriving at home. To meet this expectation, Picast can play the personalized recommendation song to you when you are arriving at home. Help you relax! 

Use Picast to detect environmental factors (humidity, temperature) and emotion of user to make personalized music recommendation. The ideal of Picast is to help reduce anxiety. When you are in a stimulating environment and want to slow down, we will choose quiet music or soothing sound such as the sound of ocean waves and brisk music. Or if you want to be stimulated because of lack of energy, we can provide dynamic music.

<p align="right">(<a href="#top">back to top</a>)</p>

### FlowChart

<img src="img/Flowchart.png">

### Built With

* Raspberry pi 4
* Power supply for RPI
* Breadboard
* Camera
* Speaker
* Temparature and humidity sensor(DHT22)
* Magnetic reed switch
* Push botton * 3
* LED * 3
* 220 Ohm resistor
* 10K resistor
* Dupont Line

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started

The hardware and software build process will be introduced separately, and a complete integration will be done at the end.

### <b>Hardware</b>

#### Circuit Design

<img src="img/circuit.png">

<p align="right">(<a href="#top">back to top</a>)</p>

### <b>Software</b>

#### Install Package

Install DHT22 package for temperature and humidity detection
```sh
  sudo pip3 install pigpio-dht
```

Install package for Emotion Detection Module
```sh
pip3 install cv2
pip3 install tensorflow
pip3 install tflite_runtime
```

Install package for Music Recommendation Module
```sh
pip3 install pandas
pip3 install numpy
pip3 install xgboost==0.90
pip3 install sklearn

Note: xgboost package version should be 0.90 for solving dependaency issue 
```
<p align="right">(<a href="#top">back to top</a>)</p>

#### Sequence Diagram

<img src="img/sequence_diagram.png">

<p align="right">(<a href="#top">back to top</a>)</p>

#### DHT22 Module

Reference: https://pypi.org/project/pigpio-dht/

Solved issue: Raspberry Pi DHT 11 sensor not working. I get None None<br>
https://stackoverflow.com/questions/66491115/raspberry-pi-dht-11-sensor-not-working-i-get-none-none

pigpio Daemon<br>
http://abyz.me.uk/rpi/pigpio/pigpiod.html

File name: <b><i>dht22.py</i></b>

* Inintialize DHT22 sensor
* Start detection
* If temperature and humidity are detected, print then
* Return `temperature` and `humidity`
```python

from pigpio_dht import DHT22
import time

def dht_detect():
    dht_gpio = 4 # BCM Numbering

    sensor = DHT22(dht_gpio)

    while True:
        try:
            result = sensor.read()
            print(result)
            
            if result["valid"] == True:
                
                temperature_c = result["temp_c"]
                humidity = result["humidity"]
                
                print(
                    "Temp: {:.1f} C ,   Humidity: {}% ".format(
                        temperature_c, humidity
                    )
                )
                return temperature_c, humidity
            
        except RuntimeError as error:
            # Errors happen fairly often, DHT's are hard to read, just keep going
            print(error.args[0])
            time.sleep(2.0)
            continue
        except Exception as error:
            sensor.exit()
            raise error

        time.sleep(1.0)
```

<p align="right">(<a href="#top">back to top</a>)</p>

#### Emotion Detection Module

Reference: https://github.com/Elliot-Bl/Emotion_Detector

File name: <b><i>picast_camera.py</i></b>

* Set face detection tool (use `haarcascade_frontalface_default.xml` from `cv2.CascadeClassifier`)
* Initialize camera and set parameters
* Genrate `emotion_mapper` and `confidence_threshold`
* Start face detection and emotion detection (for Emotion detection, use `emotions.tflite` from `tflite`)
* Show camera window display with FPS, face square
* If emotion is detected, return `emo_state`, `confidence`
```python

# Emotion detector for Raspberry Pi 4
# Author: Elliot Blanford
# Date: 1/18/2021
# Description: Just run it and make faces at the camera! It will print out predictions and
# confidence if it is above threshold. It will also buzz on detecting a change in emotional state

# Original inspiration by Evan Juras
# https://github.com/EdjeElectronics/TensorFlow-Object-Detection-on-the-Raspberry-Pi/blob/master/Object_detection_picamera.py
# I updated it to work with tensorflow v2, changed it to an emotion detection model, and added feedback device
# a vibrating motor controlled by GPIO pins

## Some of the code is copied from Google's example at
## https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb

## and some is copied from Dat Tran's example at
## https://github.com/datitran/object_detector_app/blob/master/object_detection_app.py


# Import packages
import cv2
import numpy as np
from picamera.array import PiRGBArray
from picamera import PiCamera
import tensorflow.compat.v1 as tf
import argparse
from PIL import Image
import time
import tflite_runtime.interpreter as tflite

def emotion_detect():
    cascPath = "/home/pi/.local/lib/python3.7/site-packages/cv2/data/haarcascade_frontalface_default.xml"
    faceCascade = cv2.CascadeClassifier(cascPath)

    # Set up camera constants
    IM_WIDTH = 1280//4
    IM_HEIGHT = 192 #720//4

    # Initialize frame rate calculation
    frame_rate_calc = 1
    freq = cv2.getTickFrequency()
    font = cv2.FONT_HERSHEY_SIMPLEX

    # translate model output to label
    mapper = {0:'anger', 1:'disgust', 2:'fear', 3:'happiness', 4: 'sadness', 5: 'surprise', 6: 'neutral'}

    camera = PiCamera()
    camera.resolution = (IM_WIDTH, IM_HEIGHT)
    camera.framerate = 30
    rawCapture = PiRGBArray(camera, size=(IM_WIDTH, IM_HEIGHT))

    confidence_threshold = 50 #in %

    for frame1 in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):

        t1 = cv2.getTickCount()

        # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
        # i.e. a single-column array, where each item in the column has the pixel RGB value
        frame = np.copy(frame1.array)
        frame.setflags(write=1)
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        roi_gray = frame_gray
        faces = faceCascade.detectMultiScale(
            frame_gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(50, 50),
            flags=cv2.CASCADE_SCALE_IMAGE
            )
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 3)
            box_size = max(h, w)
            roi_gray = frame_gray[y:y+box_size, x:x+box_size]
            roi_color = frame[y:y+box_size, x:x+box_size]

        face_gray = cv2.resize(roi_gray, (48,48))
        face_expanded = np.expand_dims(face_gray/255, axis=2).astype('float32')
        # Load the TFLite model and allocate tensors.
        interpreter = tflite.Interpreter(model_path="Emotion_Detector/emotions.tflite")
        interpreter.allocate_tensors()

        # Get input and output tensors.
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()

        interpreter.set_tensor(input_details[0]['index'], [face_expanded])

        interpreter.invoke()

        # The function `get_tensor()` returns a copy of the tensor data.
        # Use `tensor()` in order to get a pointer to the tensor.
        output_data = interpreter.get_tensor(output_details[0]['index'])

        confidence = np.max(output_data[0]) * 100
        # need to show predition on the screen, if it's a 'confident' prediction, i'll show the %
        if confidence > (confidence_threshold):
            emo_state = np.where(output_data[0] == np.max(output_data[0]))[0][0]
            
            camera.close()
            cv2.destroyAllWindows()
            
            return emo_state, confidence

        cv2.putText(frame, "FPS: {0:.2f}".format(frame_rate_calc), (60, 100), font, 1, (255, 255, 0), 2, cv2.LINE_AA)
        
        cv2.imshow('Emotion detector', frame)

        t2 = cv2.getTickCount()
        time1 = (t2 - t1) / freq
        frame_rate_calc = 1 / time1
        
        rawCapture.truncate(0)

        # Press 'q' to quit
        if cv2.waitKey(1) == ord('q'):
            camera.close()
            cv2.destroyAllWindows()
            return False, False

```
<p align="right">(<a href="#top">back to top</a>)</p>

#### Music Recommendation Model Training

File name: <b><i>train_model.py</i></b>

Use `RandomSearchCV` to find best parameters and use `xgboost` as classification model. The whole steps are as follows:
* Import package
* Load training data
* Build Recommendation Model
* Evaluation
* Dump model

```python

# -*- coding: utf-8 -*-
import pickle
import random
import pandas as pd
import numpy as np

from xgboost.sklearn import XGBClassifier # 用於建立RandomizedSearchCV的XGBoost
from sklearn.model_selection import StratifiedKFold, RandomizedSearchCV # 用於建立RandomizedSearchCV的XGBoost
from sklearn.model_selection import train_test_split
import xgboost as xgb # 一般的XGBoost用於和RandomizedSearchCV的XGBoost做比較

import matplotlib.pyplot as plt

mood_mapper = {0:'anger', 1:'disgust', 2:'fear', 3:'happiness', 4: 'sadness', 5: 'surprise', 6: 'neutral'}
song_mapper = {0:'pop', 1:'soft', 2:'funny', 3:'jazz', 4:'lofi'}

## Load txt data
file_object = open(r"picast_data.txt","r")
row = []
 
for line in file_object.read().splitlines():
  row.append(line.split(' '))

df = pd.DataFrame(row, columns=['temperature', 'humidity', 'emotion', 'genre'])
df = df.dropna()


## Build Recommendation Model"""

# print出XGBoost的feature importance
def print_xgb_feature_importance(feature_df, xgb_model):

  plt.barh(feature_df.columns, xgb_model.feature_importances_)
  plt.title('XGBoost feature impotance')
  plt.xlabel('importance')
  plt.ylabel('feature')
  plt.show()

input_col = ['temperature','humidity','emotion']
output_col = ["genre"]

X = df[input_col].values
y = df[output_col].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

########################### build XGBoost with RandomSearchCV ###########################
# Souce : https://www.kaggle.com/tilii7/hyperparameter-grid-search-with-xgboost
# Source2 : https://www.analyticsvidhya.com/blog/2016/03/complete-guide-parameter-tuning-xgboost-with-codes-python/

xgb_base = XGBClassifier(learning_rate=0.02, n_estimators=600, objective='binary:logistic', silent=True, nthread=1) 

# A parameter grid for XGBoost
params = {
      'min_child_weight': [1, 5, 10], # Minimum sum of instance weight (hessian) needed in a child.
      'gamma': [0, 0.1, 0.3, 0.5, 1, 1.5, 2, 5], # Minimum loss reduction required to make a further partition on a leaf node of the tree.
      'subsample': [0.6, 0.8, 1.0], # Subsample ratio of the training instances
      'colsample_bytree': [0.6, 0.8, 1.0], # subsample ratio of columns when constructing each tree
      'scale_pos_weight': [0.5, 0.6, 0.7, 1], # Control the balance of positive and negative weights
      'max_depth': range(3,10,2)
      }
folds, param_comb = 3, 5
skf = StratifiedKFold(n_splits=folds, shuffle = True, random_state = 1001)
random_search = RandomizedSearchCV(xgb_base, 
                  param_distributions=params, 
                  n_iter=param_comb, 
                  scoring='roc_auc', 
                  n_jobs=4, 
                  cv=skf.split(X_train,y_train), 
                  verbose=3, 
                  random_state=1001 )
# Here we go
random_search.fit(X_train, y_train)

################################## build normal XGBoost ##################################
xgb_model = xgb.XGBClassifier(colsample_bytree=1.0, gamma=1, learning_rate=0.02, max_depth=5,
              n_estimators=600, nthread=1, objective='multi:softprob',
              silent=True, subsample=0.6)
xgb_model.fit(X_train, y_train)


####################################### Evaluation #######################################

print_xgb_feature_importance(df[input_col], xgb_model)

result = loaded_model.score(X_test, y_test)
print(result)

filename = 'music_recommendation_picast.pkl'
pickle.dump(xgb_model, open(filename, 'wb'))

```
<p align="right">(<a href="#top">back to top</a>)</p>

#### Music Recommendation Module

File name: <b><i>recommend_music.py</i></b>

* Take classification model(`loaded_model`), ['temperature', 'humidity', 'emotion'](`input_data`) as input
* Predic music genre that user would prefer with `loaded_model` and get `predict_genre`
* Get `music_folder` from `predict_genre`
* Choose a `random_music` in `music_folder`
* Return `music_folder`, `random_music`, `predict_genre`

```python

import os
import random
import pickle
import numpy as np

# columns=['temperature', 'humidity', 'emotion']
# input_data = [['25.2','70.1','3']]

def recommend_music(loaded_model, input_data):
    
    song_mapper = {0:'pop', 1:'soft', 2:'funny', 3:'jazz', 4:'lofi'}

    # load the model from disk
    # filename = 'music_recommendation_picast.pkl'
    # loaded_model = pickle.load(open(filename, 'rb'))
    predict_genre = loaded_model.predict(np.array(input_data))[0]
    music_folder = 'song/' + song_mapper[predict_genre] + '/'
    random_music = random.choice(os.listdir(music_folder))

    #print("Recommend music: ",random_music)
    return music_folder, random_music, predict_genre

```
<p align="right">(<a href="#top">back to top</a>)</p>

#### Combine All Module

File name: <b><i>picast.py</i></b>

Reference: [Play Your Own Theme Tune When You Enter the Room With Raspberry Pi](https://www.makeuseof.com/tag/play-theme-tune-enter-room-raspberry-pi/)

The detailed implementation functions have been defined on each module, and the connection relationship between functions have been defined on the <a href="#sequence-diagram">Sequence Diagram</a>
<br>

According to the <a href="#flowchart">FlowChart</a>, there are the following functions in <b><i>picast.py</i></b>, which correspond to the steps in the <a href="#flowchart">FlowChart</a> in order. Besides, the hardware need to be controlled by GPIO and Raspberry Pi or trigger some function. The details and impletments are as follows:

Note that we set GPIO mode is `BOARD`
`GPIO.setmode(GPIO.BOARD)`

|     Function    |       Flow      |     Trigger     |
| --------------- | --------------- | --------------- |
| activeState() | Activate Picast | Button on Board pin 11|
| watchDoor() | Open Door Detection | Magnetic reed switch on Board pin 15 |
| feedback_detection() | Save Feedback | Button on Board pin 29 |
| feedback_detection() | Pause Music | Button on Board pin 13 |

<p align="right">(<a href="#top">back to top</a>)</p>

## Usage

### Demo Video

Please refer to the [Picast Demo Video](https://youtu.be/cIBvNV6wCOU)(click on picture!)

[![Picast Demo Video](http://img.youtube.com/vi/cIBvNV6wCOU/0.jpg)](http://www.youtube.com/watch?v=cIBvNV6wCOU "Picast Demo — Entrance Music with Recommendation Model")

<p align="right">(<a href="#top">back to top</a>)</p>

## Reference

* [DHT22 python package](https://pypi.org/project/pigpio-dht/)

* [Solved issue: Raspberry Pi DHT 11 sensor not working. I get None None](https://stackoverflow.com/questions/66491115/raspberry-pi-dht-11-sensor-not-working-i-get-none-none)

* [pigpio Daemon](http://abyz.me.uk/rpi/pigpio/pigpiod.html)

* [Emotion Detection Module](https://github.com/Elliot-Bl/Emotion_Detector)

* [Play Your Own Theme Tune When You Enter the Room With Raspberry Pi](https://www.makeuseof.com/tag/play-theme-tune-enter-room-raspberry-pi/)

